from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand
import pygments_aql

setup(
    name="pygments-aql",
    version=pygments_aql.__version__,
    description="Pygments lexer for Arango Query Language (AQL)",
    long_description=open("README.md").read(),
    url="https://gitlab.com/chrdudek/pygments-aql",
    author="Dr. Christian-Alexander Dudek",
    author_email="christian@dudek.me",
    license="MIT",
    classifiers=[
        "Environment :: Plugins",
        "License :: OSI Approved :: MIT License",
    ],
    keywords="pygments shader aql arangodb pygments-lexer",
    packages=find_packages(exclude=["tests*"]),
    install_requires=["Pygments"],
    entry_points={"pygments.lexers": "aql=pygments-aql:AqlLexer"},
    tests_require=["pytest"],
)
