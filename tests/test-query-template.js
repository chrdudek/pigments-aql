const { query } = require("@arangodb")
const max = 13
const oddNumbers = query`FOR i IN 1..${Math.sum([1, 2, 3, 4])}
  FILTER i % @num == 1
  RETURN i
`.toArray()
console.log(`The numbers are: ${oddNumbers}`) // 1,3,5,7,9,11,13
