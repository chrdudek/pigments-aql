# -*- coding: utf-8 -*-
from pygments.lexers.javascript import JavascriptLexer
from pygments.lexer import bygroups, using, inherit

from .AqlLexer import AqlLexer


class JavascriptAqlLexer(JavascriptLexer):
    name = "AQL Javascript Lexer"
    aliases = ["aql-js"]
    filenames = ["*.js"]
    tokens = {
        "root": [
            ("(query`)(.*)(`)", bygroups(inherit, using(AqlLexer), inherit))
        ]
    }
